# Site web pour un namespace

## Namespace

Un namespace est soit un utilisateur, soit un groupe.

Il est possible de créer un site web "vitrine" pour un namespace. Cela permet d'avoir une URL plus courte du genre :
`https://<NAMESPACE>.gitlabpages.inria.fr/`

## Renommer son repository

Pour cela, il faut renommer son repository en `<NAMESPACE>.gitlabpages.inria.fr`.
Il faut aller dans Settings/General/Advanced
![rename repository](RenameRepository.png)

## Résultat

Pour vérifier que tout s'est bien passé, il faut aller dans Settings/Pages
![namespace web site](NamespaceWebsite.png)
