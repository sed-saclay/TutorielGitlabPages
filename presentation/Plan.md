# DLP - Gitlab Pages

Activé sur l'instance gitlab d'inria depuis juin 2018

## Étapes de configuration

- [Activation des pages et lien avec l'intégration continue](Setup.md)

- [Activation des pages de namespace](Namespace.md)

## À quoi ça sert

- Mettre en ligne des sites web statiques : documentation, outil web côté client, 
blog...

- Destiné à du contenu public : pas de contrôle d'accès 

## Comment on s'en sert

- Avoir un projet sur gitlab.inria.fr
- Activer l'intégration continue
- Disposer d'un runner gitlab ou le créer 
  * éventuellement via ci.inria.fr
  * pour l'instant, pas de runners partagés
  * runner docker : pour garder toute la configuration de build dans le fichier `.gitlab-ci.yml` (le runner lui-même est sans état)
  * autre possibilité : runner shell. Dans ce cas l'état du runner aura plus d'importance
- Créer un fichier `.gitlab-ci.yml` contenant les instructions de build
- La construction et le déploiement se font quand une branche est poussée

## Exemple - démo

## Site web pour un namespace

## Gotchas - attention

Le chemin des pages *doit* s'appeler `public` : voir [cette page](https://docs.gitlab.com/ce/user/project/pages/getting_started_part_four.html#the-public-directory)

Configuration de runners Docker pour ci.inria.fr : voir [cette précision](https://gitlab.inria.fr/siteadmin/doc/wikis/faq#using-a-docker-executor) sur le wiki du gitlab inria.

## Trucs et astuces

- Secrets dans Gitlab

## Évolutions

Runners partagés ? Les besoins de ressources pour la génération de sites statiques sont souvent modestes, et cela simplifierait grandement la configuration initiale. À suivre...

## Liens

- Documentation inria:
  * [Utilisation du service d'intégration continue de gitlab avec ci.inria.fr](https://gitlab.inria.fr/siteadmin/doc/wikis/faq#how-to-use-the-continuous-integration-ci-service)
- [Documentation](https://docs.gitlab.com/ee/user/project/pages/) gitlab ([exemples](https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/) avec les générateurs de sites statiques les plus courants)
