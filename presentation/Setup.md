# Configuration de gitlab pages

## Sur le projet gitlab

- Activer l'intégration continue

Dans Settings -> General -> Permissions, activer les Pipelines. Ceci donne accès à la configuration CI / CD

![Activation des pipelines](setup_enable_pipelines.png)

- Récupérer le *runner token* dans Settings -> CI / CD -> General pipelines

![Récupération du runner token](setup_runner_token.png)

## Création d'un runner basé sur docker

- Créer ou utiliser un projet sur [ci.inria.fr](https://ci.inria.fr)
- Créer une machine virtuelle (ou utiliser une machine virtuelle existante)
- Installer Docker ([instructions pour Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/) par exemple)
- [Installer le runner gitlab](https://docs.gitlab.com/runner/install/)
- [Configurer le runner gitlab et le lier au projet](https://gitlab.inria.fr/siteadmin/doc/wikis/faq#register-a-runner-in-your-project)
  * Utiliser le *runner token* obtenu précédemment
  * L'URL gitlab est `https://gitlab.inria.fr`
  * Choisir 'docker' comme executeur
  * Modifier le fichier `/etc/gitlab-runner/config.toml` pour ajouter `network_mode = "host"` dans la configuration du runner comme indiqué [ici](https://gitlab.inria.fr/siteadmin/doc/wikis/faq#using-a-docker-executor)
  * Relancer gitlab-runner (`sudo gitlab-runner restart`)

## Vérifier que le runner est visible et actif

- Il doit apparaître dans Settings -> CI / CD -> Runners

![Vérification du runner](setup_check_runner_active.png)
